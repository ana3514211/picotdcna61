/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   deltaContainer.cpp
   Author: avdmit

   Created on January 28, 2020, 10:53 AM
*/

#include "deltaContainer.h"

deltaContainer::deltaContainer()
{
}

deltaContainer::deltaContainer(short iPlaneFirst, short iPlaneSecond,
                               short iStripFirst, short iStripSecond,
                               double iWidthFirst, double iWidthSecond,
                               double iTime, double iPosX, int ipps)
{

  fPlaneFirst = iPlaneFirst;
  fStripFirst = iStripFirst;
  fWidthFirst = iWidthFirst;

  fPlaneSecond = iPlaneSecond;
  fStripSecond = iStripSecond;
  fWidthSecond = iWidthSecond;

  fTime = iTime;
  fPosX = iPosX;
  fpps = ipps;
}

ClassImp(deltaContainer);
