## -*- Makefile -*-
##
## User: aldmitri
## Time: NoV 09, 2023 11:42:36 AM
##


SOURCES := $(wildcard *.cpp) Dict.cxx
HEADERS := $(wildcard *.h)
EXE := $(patsubst %.cc,%, $(wildcard *.cc))
DICTIONARY=Dict.cxx

all: $(EXE) $(DICTIONARY)

$(DICTIONARY):
	rootcling -f $(DICTIONARY) -c $(HEADERS)

$(EXE): %: %.cc
	g++ -g -o $@ $< $(SOURCES) `root-config --cflags --glibs ` -lMathMore

clean: 
	rm -rf $(EXE) Dict*
	rootcling -f $(DICTIONARY) -c $(HEADERS)
