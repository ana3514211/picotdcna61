/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   deltaContainer.h
   Author: avdmit

   Created on January 28, 2020, 10:53 AM
*/

#ifndef DELTACONTAINER_H
#define DELTACONTAINER_H

#include "TObject.h"

using namespace std;

class deltaContainer : public TObject {
public:
  /** Default constructor **/
  deltaContainer();

  /** Constructor to use **/
  deltaContainer(short iPlaneFirst, short iPlaneSecond,
                 short iStripFirst, short iStripSecond,
                 double iWidthFirst, double iWidthSecond,
                 double iTime, double iPosX, int ipps);

  void SetPlaneFirst(short tmp)   {  fPlaneFirst = tmp;   }
  void SetStripFirst(short tmp)   {  fStripFirst = tmp;   }
  void SetWidthFirst(double tmp)  {  fWidthFirst = tmp;   }
  void SetPlaneSecond(short tmp)  {  fPlaneSecond = tmp;  }
  void SetStripSecond(short tmp)  {  fStripSecond = tmp;  }
  void SetWidthSecond(double tmp) {  fWidthSecond = tmp;  }

  void SetTime(double tmp)        {  fTime = tmp;         }
  void SetPosX(double tmp)        {  fPosX = tmp;         }
  void SetPPS(double tmp)         {  fpps = tmp;          }

  short GetPlaneFirst()           {  return fPlaneFirst;  }
  short GetStripFirst()           {  return fStripFirst;  }
  short GetPlaneSecond()          {  return fPlaneSecond; }
  short GetStripSecond()          {  return fStripSecond; }
  double GetWidthFirst()          {  return fWidthFirst;  }
  double GetWidthSecond()         {  return fWidthSecond; }

  double GetTime()                {  return fTime;        }
  double GetPosX()                {  return fPosX;        }
  double GetPPS()                 {  return fpps;         }

  /** Destructor **/
  virtual ~deltaContainer() {};

private:
  short fPlaneFirst;
  short fStripFirst;
  double fWidthFirst;
  short fPlaneSecond;
  short fStripSecond;
  double fWidthSecond;

  double fTime;
  double fPosX;
  int fpps;

  ClassDef(deltaContainer, 1);
};

#endif /* DELTACONTAINER_H */
