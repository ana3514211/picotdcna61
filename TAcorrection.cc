/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   main.cpp
   Author: avdmit

   Created on November 13, 2023, 14:07 PM
*/

#include <TApplication.h>
#include <TChain.h>
#include <TClonesArray.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TH1.h>
#include <TH2.h>
#include <TGraph.h>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <unordered_map>
#include <unistd.h>
#include <chrono>
#include <thread>

#include "deltaContainer.h"

using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono; // nanoseconds, system_clock, seconds

TString FormPat(TString nf)
{
  char currentdir[1024];
  chdir(nf.Data());
  getcwd(currentdir, sizeof(currentdir));
  TString path = Form("%s", currentdir);

  if (nf.Sizeof() == 0) {
    printf("Path to data file is \n %s \n", path.Data());
    return path;
  }
  printf("Path to data file is \n %s \n", path.Data());
  return path;
}

void Analysis(Int_t run_number, bool verbose = 0, short version = 0)
{

  TString name;
  short nPlanes = 3;
  vector<short> iStripNINO = {6, 3, 3};
  vector<short> iStripAnalogue = {5, 3, 3};
  auto* iStripRef = &iStripAnalogue;
  if (run_number > 5)
    iStripRef = &iStripNINO;

  int pps = (iStripRef->at(2) << 8) + (iStripRef->at(1) << 4) + iStripRef->at(0);
  //
  // Analogue readout
  //
  // vector <std::pair<double, double>> amplitudeCuts = {{2.7, 3}, {3.5, 3.8}};
  int fX = 0, sX = 25;
  // int maxStep = 7;
  // int maxEntries = 90;
  //
  // int nBinsX = 500, nBinsY = 150;
  // int fY = -1, sY = 4;
  // TString amplitude = "First";
  //
  // int nBinsX = 500, nBinsY = 90;
  // int fY = -1.5, sY = 1.5;
  // TString amplitude = "Second";

  vector <std::pair<double, double>> amplitudeCutsNINO = {{16.75, 18.25}, {18.5, 20.5}, {20.2, 21.5}}; // 0 1 2
  vector <std::pair<double, double>> amplitudeCutsAnalogue = {{1.25, 1.5}, {2.7, 3.0}, {3.5, 3.8}};
  auto* amplitudeCutsRef = &amplitudeCutsAnalogue;
  if (run_number > 5)
    amplitudeCutsRef = &amplitudeCutsNINO;

  std::pair<double, double> posXCutsNINO = {-3.0, -0.5};
  std::pair<double, double> posXCutsAnalogue = {2.0, 6.0};
  auto* posXCutsRef = &posXCutsAnalogue;
  if (run_number > 5)
    posXCutsRef = &posXCutsNINO;

  // int fX = 0, sX = 60;
  int maxStep = 150;
  int maxEntries = 60;
  //
  int nBinsX = 1200, nBinsY = 240;
  int fY = -4, sY = 4;
  vector <TString> amplitudes = {"First", "Second"};
  TString amplitude = amplitudes.at(version % amplitudes.size());

  printf("Start analysis\n");
  TString PathToInFiles = "/home/alex/MyGitlab/picoTDC/analysis/.";
  PathToInFiles         = FormPat(PathToInFiles);
  TString FileName      = Form("%s/run%i_picoTDC_delta", PathToInFiles.Data(), run_number); // NameInFile
  FileName += ".root"; // NameInFile

  cout << "\nOpen file \n " << FileName << endl << endl;

  TClonesArray* dtArray = new TClonesArray("deltaContainer");
  Int_t ResultSetBranch, ResultAdd;
  Long64_t nEvForRead;

  // Read tree from file
  TChain* eveTree = new TChain("picoTDC");
  ResultAdd          = eveTree->Add(FileName, 0);
  cout << "total number of files connected = " << ResultAdd << endl;
  if (ResultAdd != 1) {
    cout << Form("Error opening files %s\n", FileName.Data()) << endl;
    return;
  }
  ResultSetBranch = eveTree->SetBranchAddress("delta", &dtArray);
  printf("delta Set Branch = %i\n", ResultSetBranch);
  ResultSetBranch = -1;
  // ResultSetBranch = eveTree->SetBranchAddress("pps", &pps);
  printf("nHits Set Branch = %i\n", ResultSetBranch);
  ResultSetBranch = -1;
  nEvForRead      = eveTree->GetEntries();
  cout << "In the picoTDC file " << nEvForRead << " events \n" << endl;

  // Create outFile.root in saveFolder
  FileName = Form("%s/run%i_picoTDC_TA_v%i", PathToInFiles.Data(), run_number,  version); // NameOutFile
  FileName += ".root"; // NameOutFile
  printf("Output file \n %s\n\n", FileName.Data());
  TFile* fileout = new TFile(FileName, "RECREATE");

  // Draw settings
  TCanvas* c =  NULL;
  if (verbose) {
    // c = new TCanvas("c", "The Canvas", 0, 0, 1440, 1000);
    c = new TCanvas("c", "The Canvas", 0, 0, 1900, 1000);
    c->Divide(2, 1);
  }
  TF1* gaus = new TF1("gaus", "gaus");
  vector<TGraph*> graphs ;

  for (short iPlf = 0 ; iPlf < nPlanes; ++iPlf) {
    for (short iPls = 0 ; iPls < nPlanes; ++iPls) {
      if (iPls >= iPlf)
        continue;

      if (verbose)
        c->Clear("D");

      short iStf = iStripRef->at(iPlf);
      short iSts = iStripRef->at(iPls);

      TString cutsF = Form("delta.fPlaneFirst==%i && delta.fStripFirst==%i", iPlf, iStf);
      TString cutsS = Form("delta.fPlaneSecond==%i && delta.fStripSecond==%i", iPls, iSts);
      TString cutsPosX = Form("delta.fPosX>%f && delta.fPosX<%f ", posXCutsRef->first, posXCutsRef->second);
      TString cutsWithPosX = Form("%s && %s && %s && delta.fpps==%i", cutsF.Data(), cutsS.Data(), cutsPosX.Data(), pps);
      TString cutsWithoutPosX = Form("%s && %s && delta.fpps==%i", cutsF.Data(), cutsS.Data(), pps);
      auto cuts = cutsWithPosX;
      if (iPlf == 2 && iPls == 1)
        cuts = cutsWithoutPosX;

      //
      // USE THIS CUT FOR FIRST TIME ONLY
      //
      if (version < 2) {
        if (strcmp(amplitude.Data(), "First") == 0)
          cuts += Form(" && delta.fWidthSecond>%f && delta.fWidthSecond<%f",
                       amplitudeCutsRef->at(iPls).first,
                       amplitudeCutsRef->at(iPls).second);
        if (strcmp(amplitude.Data(), "Second") == 0)
          cuts += Form(" && delta.fWidthFirst>%f && delta.fWidthFirst<%f",
                       amplitudeCutsRef->at(iPlf).first,
                       amplitudeCutsRef->at(iPlf).second);
      }

      TString graphName = Form("dtPl%iPl%i_vs_widthPl%s", iPlf, iPls, amplitude.Data());
      TString sHist = Form("delta.fTime:delta.fWidth%s>>hnew(%i,%i,%i,%i,%i,%i)",
                           amplitude.Data(), nBinsX, fX, sX, nBinsY, fY, sY);

      if (verbose)
        cout << "cuts: " << cuts.Data() << endl;

      graphs.push_back(new TGraph());
      graphs.back()->SetNameTitle(graphName.Data(), graphName.Data());
      // graph->SetLineWidth(3);
      graphs.back()->SetMarkerStyle(21);
      graphs.back()->SetMarkerColor(2);
      graphs.back()->SetMarkerSize(.5);

      eveTree->Draw(sHist.Data(), cuts.Data(), "");
      // get hnew from the current directory
      TH2D* hnew = (TH2D*)gDirectory->Get("hnew");
      if (verbose) {
        c->cd(1);
        hnew->Draw("colz");

      }

      for (Int_t firstBin = 0; firstBin < nBinsX; ++firstBin) {
        for (Int_t lastBin = firstBin + 1; lastBin < nBinsX && lastBin - firstBin < maxStep; ++lastBin) {
          TH1D* hnewY = hnew->ProjectionY(Form("_py_%i_%i", firstBin, lastBin), firstBin, lastBin, "");
          if (hnewY->GetEffectiveEntries() < maxEntries)
            continue;

          double maxX = (double)hnewY->GetMaximumBin() * (abs(fY) + abs(sY)) / nBinsY + fY;
          double measX = ((double)(lastBin + firstBin) / 2) * (abs(fX) + abs(sX)) / nBinsX + fX;
          if (verbose) {
            // cout << "first: " << firstBin << endl;
            // cout << "last:  " << lastBin << endl;
            // cout << "entries:  " << hnewY->GetEntries() << endl;
            // cout << "effentr:  " << hnewY->GetEffectiveEntries() << endl;
            // cout << "maxbin:  " << hnewY->GetMaximumBin() << endl;
            // cout << "maxX:  " << maxX << endl;
            // cout << "measX:  " << measX << endl;
            // cout << "measY:  " << gaus->GetParameter(1) << endl;
            // cout << "-----------------------\n" << endl;

            c->cd(2);
            hnewY->Draw("");
          }

          hnewY->Fit(gaus, "WQ", "", maxX - .85, maxX + .85);
          graphs.back()->AddPoint(measX, gaus->GetParameter(1));

          if (verbose) {
            c->cd(1);
            graphs.back()->Draw("PL");

            c->Update();
            sleep_until(system_clock::now() + milliseconds(3));
            // getchar();
          }
          break;
        } // end of loop over last bin
      } //end of loop over first bin
      if (verbose)
        getchar();
    } //end of loop over second plane
  } //end of loop over first plane
  for (auto i : graphs)
    i->Write();
  fileout->Close();
}

int main(int argc, char** argv)
{

  Int_t run_number;
  short version = 0;
  bool verbose = 0;
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] <<
              " <run number> <verboseMode, default is 0>, <version, default is 0>" << std::endl;
    return 1;
  }
  else if (argc >= 2)
    run_number = atoi(argv[1]);
  if (argc >= 3)
    verbose  = atoi(argv[2]);
  if (argc > 3)
    version  = atoi(argv[3]);
  std::cout << "Doing analysis for run " << run_number << "\n";
  TApplication theApp("App", &argc, argv);
  Analysis(run_number, verbose, version);
  std::cout << "Analysis complete!" << std::endl;
  return 0;

} // end int main
