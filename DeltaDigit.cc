/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   main.cpp
   Author: avdmit

   Created on November 13, 2023, 14:07 PM
*/

#include <TApplication.h>
#include <TChain.h>
#include <TClonesArray.h>
#include <TFile.h>
#include <TH1.h>
#include <TSystem.h>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <unordered_map>
#include <unistd.h>
#include <tuple>

#include "picoHit.h"
#include "deltaContainer.h"
#include "AnalysisClass.h"

using namespace std;

TString FormPat(TString nf)
{
  char currentdir[1024];
  chdir(nf.Data());
  getcwd(currentdir, sizeof(currentdir));
  TString path = Form("%s", currentdir);

  if (nf.Sizeof() == 0) {
    printf("Path to data file is \n %s \n", path.Data());
    return path;
  }
  printf("Path to data file is \n %s \n", path.Data());
  return path;
}

bool checkCluster(int idigSet,  TClonesArray** hitArray)
{
  double maxDistance = sqrt(1.75 * 1.75 + 3.5 * 3.5); // circle
  // double maxDistance = 3.5; // cross
  // double maxDistance = 1.75; // up/down
  // cout << "checking cluster" << endl;

  picoHit* digSet = (picoHit*)(*hitArray)->At(idigSet);
  int planeSet = digSet->GetPlane();
  for (Int_t idig = 0; idig < (*hitArray)->GetEntriesFast(); ++idig) {
    picoHit* dig = (picoHit*)(*hitArray)->At(idig);
    if (dig != digSet)
      if (dig->GetPlane() == planeSet) {
        double dX = abs(dig->GetPosX() - digSet->GetPosX());
        double dY = abs(dig->GetPosY() - digSet->GetPosY());
        double distance = sqrt(dX * dX + dY * dY);
        // cout << "distance: " << distance << endl;
        if (distance <= maxDistance)
          return true;
      }
  }
  return false;
}

void Analysis(Int_t run_number, bool verbose = 0)
{

  TString name;
  short nPlanes = 3;
  vector<short> nStrips = {8, 16, 16};
  vector<short> iStripNINO = {6, 3, 3};
  vector<short> iStripAnalogue = {5, 3, 3};
  auto* iStripRef = &iStripAnalogue;
  if (run_number > 5)
    iStripRef = &iStripNINO;
  vector<short> nSides = {2, 1, 1};
  bool neighbourhood = false;
  double nTrigger = 0;
  double nFiredStrip = 0;
  double eff;

  std::pair<double, double> posXCutsNINO = {-3.0, -0.5};
  std::pair<double, double> posXCutsAnalogue = {2.0, 6.0};
  auto* posXCutsRef = &posXCutsAnalogue;
  if (run_number > 5)
    posXCutsRef = &posXCutsNINO;

  printf("Start analysis\n");
  TString PathToInFiles = "/home/alex/MyGitlab/picoTDC/analysis/.";
  PathToInFiles         = FormPat(PathToInFiles);
  TString FileName      = Form("%s/run%i_picoTDC_ana", PathToInFiles.Data(), run_number); // NameInFile
  FileName += ".root"; // NameInFile

  cout << "\nOpen file \n " << FileName << endl << endl;

  vector<TClonesArray*> hitArrays = {new TClonesArray("picoHit"),
                                     new TClonesArray("picoHit"),
                                     new TClonesArray("picoHit")
                                    };
  Int_t ResultSetBranch, ResultAdd;
  Long64_t nEvForRead;
  short nHit;

  // Read tree from file
  TChain* eveTree = new TChain("picoTDC");
  ResultAdd          = eveTree->Add(FileName, 0);
  cout << "total number of files connected = " << ResultAdd << endl;
  if (ResultAdd != 1) {
    cout << Form("Error opening files %s\n", FileName.Data()) << endl;
    return;
  }
  for (short iPl = 0 ; iPl < nPlanes; ++iPl) {
    ResultSetBranch = eveTree->SetBranchAddress(Form("picoHits%i", iPl), &hitArrays.at(iPl));
    printf("picoHits%i Set Branch = %i\n", iPl, ResultSetBranch);
    ResultSetBranch = -1;
  }
  ResultSetBranch = eveTree->SetBranchAddress("nHits", &nHit);
  printf("nHits Set Branch = %i\n", ResultSetBranch);
  ResultSetBranch = -1;
  nEvForRead      = eveTree->GetEntries();
  cout << "In the picoTDC file " << nEvForRead << " events \n" << endl;

  // Create outFile.root in saveFolder
  FileName = Form("%s/run%i_picoTDC_delta", PathToInFiles.Data(), run_number); // NameOutFile
  FileName += ".root"; // NameOutFile
  printf("Output file \n %s\n\n", FileName.Data());
  TFile* fileout = new TFile(FileName, "RECREATE");
  TTree* t_out   = new TTree("picoTDC", "picoTDC");

  TClonesArray* dtArray = new TClonesArray("deltaContainer");
  TClonesArray* dtStripArray = new TClonesArray("deltaContainer");
  int pps = -1;

  int nHit0 = 0, nHit1 = 0, nHit2 = 0;
  t_out->Branch("delta", &dtArray);
  if (neighbourhood)
    t_out->Branch("deltaS", &dtStripArray);
  // t_out->Branch("pps", &pps);

  //Set TA correction
  AnalysisClass ana;
  short version = 0;
  FileName = Form("%s/run%i_picoTDC_TA_v%i", PathToInFiles.Data(), run_number, version); // NameInFile
  FileName += ".root"; // NameInFile

  while (!gSystem->AccessPathName(FileName)) {
    cout << "Setting TA correction from: " << FileName.Data() << endl;
    ana.SetTACorrection(FileName);

    ++version;
    FileName = Form("%s/run%i_picoTDC_TA_v%i", PathToInFiles.Data(), run_number, version); // NameInFile
    FileName += ".root"; // NameInFile
  }

  for (Int_t iEv = 0; iEv < /* 5000;  */ nEvForRead; iEv++) {
    dtArray->Clear();
    dtStripArray->Clear();
    for (short iPl = 0 ; iPl < nPlanes; ++iPl)
      hitArrays.at(iPl)->Clear();

    eveTree->GetEntry(iEv);
    if (iEv % 50000 == 0)
      cout << iEv / 1000 << "K Events have been read" << endl;

    if (verbose) {
      cout << "====================== EVENT " << iEv  << " ======================" << endl;
      // for (Int_t idig = 0; idig < hitArray->GetEntriesFast(); ++idig) {
      // cout << ((picoHit*) hitArray->At(idig))->GetPlane() << " - " << ((picoHit*) hitArray->At(idig))->GetStrip() << endl;
      // }
      // cout << "\nanalysis" << endl;
    }

    // //skip events with less than three hits
    // if (nHit == 3)

    for (int idig2 = 0; idig2 < hitArrays.at(2)->GetEntriesFast(); ++idig2) {
      picoHit* dig2 = (picoHit*) hitArrays.at(2)->At(idig2);
      if (verbose)
        cout << "2 - " << idig2 << " " << endl;
      bool cluster2 = dig2->GetCluster();

      if (!cluster2 && dig2->GetStrip() == iStripRef->at(dig2->GetPlane()))
        for (int idig1 = 0; idig1 < hitArrays.at(1)->GetEntriesFast(); ++idig1) {
          picoHit* dig1 = (picoHit*) hitArrays.at(1)->At(idig1);
          if (verbose)
            cout << "1 - " << idig1 << " " << endl;
          bool cluster1 = dig1->GetCluster();

          if (!cluster1 && dig1->GetStrip() == iStripRef->at(dig1->GetPlane())) {
            ++nTrigger;
            int effKey = false;
            for (int idig0 = 0; idig0 < hitArrays.at(0)->GetEntriesFast(); ++idig0) {
              picoHit* dig0 = (picoHit*) hitArrays.at(0)->At(idig0);
              if (verbose)
                cout << "0 - " << idig0 << " " << endl;
              bool cluster0 = dig0->GetCluster();

              pps = (dig2->GetStrip() << 8) + (dig1->GetStrip() << 4) + dig0->GetStrip();
              if (abs(dig0->GetStrip() -  iStripRef->at(dig0->GetPlane())) <= 1
                  && !effKey) {
                // if (dig1->GetPosX() - dig0->GetPosX() > posXCutsRef->first &&
                //     dig1->GetPosX() - dig0->GetPosX() < posXCutsRef->second) {
                //
                if (dig0->GetTimeT() > 78 ||
                    dig0->GetTimeB() > 78) {
                  ++nFiredStrip;
                  effKey = true;
                }
              }

              new ((*dtArray)[dtArray->GetEntriesFast()]) deltaContainer(
                dig2->GetPlane(), dig1->GetPlane(),
                dig2->GetStrip(), dig1->GetStrip(),
                dig2->GetWidth(), dig1->GetWidth(),
                dig2->GetTime() - dig1->GetTime() - ana.GetTACorrection(2, 1, dig2->GetWidth(), dig1->GetWidth()),
                dig2->GetPosX() - dig1->GetPosX(), pps);

              new ((*dtArray)[dtArray->GetEntriesFast()]) deltaContainer(
                dig2->GetPlane(), dig0->GetPlane(),
                dig2->GetStrip(), dig0->GetStrip(),
                dig2->GetWidth(), dig0->GetWidth(),
                dig2->GetTime() - dig0->GetTime() - ana.GetTACorrection(2, 0, dig2->GetWidth(), dig0->GetWidth()),
                dig2->GetPosX() - dig0->GetPosX(), pps);

              new ((*dtArray)[dtArray->GetEntriesFast()]) deltaContainer(
                dig1->GetPlane(), dig0->GetPlane(),
                dig1->GetStrip(), dig0->GetStrip(),
                dig1->GetWidth(), dig0->GetWidth(),
                dig1->GetTime() - dig0->GetTime() - ana.GetTACorrection(1, 0, dig1->GetWidth(), dig0->GetWidth()),
                dig1->GetPosX() - dig0->GetPosX(), pps);

            }
          } // end of loop over expected MRPC hits
        } // end of loop over expected Pad1 hits
    } // end of loop over expected Pad2 hits

    //       } // end of loop over the Plane0 digits
    //   } // end of loop over the Plane1 digits
    // } // end of loop over the Plane2 digits

    // for (int iPl = nPlanes - 1 ; iPl > -1; --iPl) {
    //   Int_t idig = 0;
    //   picoHit* dig = (picoHit*) picoHit->At(idig);
    //   for (Int_t idig = 0; idig < hitArray->GetEntriesFast(); ++idig) {
    //     digs.at(iPl) = dig;

    //   }
    //   if (((picoHit*) hitArray->At(idig))->GetPlane() != iPl) {
    //     if (nSkip.at(iPl) == 0)
    //       digs.at(iPl) = (picoHit*) hitArray->At(idig);
    //     nSkip.at(iPl) -= 1;
    //   }

    // } // end of loop over digits

    // std::get<0>(tpl) = 1;
    // std::get<1>(tpl) = 2;
    // std::get<2>(tpl) = 3;
    // Int_t idig = 0;
    // picoHit* dig0 = (picoHit*) hitArray->At(idig);
    // picoHit* dig1 = NULL;
    // picoHit* dig2 = NULL;

    //     (picoHit*) hitArray->At(get<0>(idig)),
    //     if (dig->GetPlane() != iPl)
    //     if (dig->GetStrip() != iSt)
    //       continue;

    //   // } //end of loop over strips
    // } //end of loop over planes
    // getchar();
    t_out->Fill();
  } //end of loop over events

  cout << "eff: " << nFiredStrip / nTrigger << " = ( " << nFiredStrip << "/" << nTrigger << " )" << endl;

  fileout->cd();
  // t_out->Print();
  t_out->Write();
  fileout->Close();
}

int main(int argc, char** argv)
{

  Int_t run_number;
  bool verbose = 0;
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " <run number> <verboseMode, default is 0>" << std::endl;
    return 1;
  }
  else if (argc >= 2)
    run_number = atoi(argv[1]);
  if (argc > 2)
    verbose  = atoi(argv[2]);
  std::cout << "Doing analysis for run " << run_number << "\n";
  TApplication theApp("App", &argc, argv);
  Analysis(run_number, verbose);
  std::cout << "Analysis complete!" << std::endl;
  return 0;

} // end int main
