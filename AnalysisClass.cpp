/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   AnalysisClass.cpp
   Author: avdmit

   Created on January 07, 2020, 10:05 AM
*/

#include "AnalysisClass.h"

AnalysisClass::AnalysisClass()
{
  fTime.clear();
  fWidth.clear();
  fCorrTB.clear();
  fZ.clear();
  fHistLists.assign(fNStr, NULL);

  for (Int_t i = 0; i < fNStr; i++) {
    fHistLists[i] = new TList();
    hdW_full[i] = new TH1D(Form("dW_full_str_%d", i), Form("dW_full_str_%d", i), 1024, -32, 32);
    hdW_cut[i] = new TH1D(Form("dW_cut_str_%d", i), Form("dW_cut_str_%d", i), 1024, -32, 32);
    hPos_full[i] = new TH1D(Form("pos_full_str_%d", i), Form("pos_full_str_%d", i), 1536, -24, 24);
    hPos_cut[i] = new TH1D(Form("pos_cut_str_%d", i), Form("pos_cut_str_%d", i), 1536, -24, 24);
    hdT_full[i] = new TH1D(Form("dT_full_str_%d", i), Form("dT_full_str_%d", i), 1024, 0, 128);
    hdT_cut[i] = new TH1D(Form("dT_cut_str_%d", i), Form("dT_cut_str_%d", i), 1024, 0, 128);
    fHistLists[i]->Add(hdW_full[i]);
    fHistLists[i]->Add(hdW_cut[i]);
    fHistLists[i]->Add(hPos_full[i]);
    fHistLists[i]->Add(hPos_cut[i]);
    fHistLists[i]->Add(hdT_full[i]);
    fHistLists[i]->Add(hdT_cut[i]);
  }

  // analogue readout
  // fCorrTB.insert(fCorrTB.end(),
  // {0.1321875, 0.0234375, 0.0215625, -0.04875, -0.015, -0.11625, -0.046875, -0.1734375});

  // NINO readout
  fCorrTB.insert(fCorrTB.end(),
  {-0.06525, -0.173625, -0.00825, -0.005625, -0.094875, -0.051, 0.056625, 0.0105});
}

void AnalysisClass::Clear()
{
  fTimeTB = 0;
  fWidthTB = 0;
  fPos.clear();
  // fPos.assign(3, 0.);
  fTime.assign(2, 0.);
  fWidth.assign(2, 0.);
}

void AnalysisClass::Init(int run_number)
{
  if (run_number > 5) {
    fPosXStripMax = 7;
    fPosXStripMin = -0.5;
  }
  else {
    fPosXStripMax = 8.5;
    fPosXStripMin = -3;
  }
}

Bool_t AnalysisClass::pushDigi(picoDigit* dig, bool verbose = 0)
{
  fPlane = dig->GetPlane();
  fStrip = dig->GetStrip();
  fSide = dig->GetSide();
  fTime.at(fSide) = dig->GetTime();
  fWidth.at(fSide) = dig->GetWidth();

  // Pads only
  if (fPlane != 0)
    return setPadDigi(verbose);
  // Strips only
  else
    return setStripDigi(verbose);

}

Bool_t AnalysisClass::setPadDigi(bool verbose)
{
  fTimeTB = fTime.at(fSide);
  fWidthTB = fWidth.at(fSide);
  fPos.push_back(6.125 - (fStrip % 8) * 1.75);  // X
  fPos.push_back(-1.75 + (fStrip / 8) * 3.5);    // Y
  fPos.push_back(20.0 / fPlane);                 // Z
  if (fTimeTB < 78) {
    Clear();
    return kFALSE;
  }

  if (verbose) {
    cout << "Setting #" << fPlane << "Pad#" << fStrip
         << "\nfinal timeTB " << fTimeTB
         << "\nfinal widthTB " << fWidthTB
         << "\nfinal pos " << fPos.at(0) << " " << fPos.at(1) << " " << fPos.at(0) <<  "\n";
  }
  return kTRUE;
}

Bool_t AnalysisClass::setStripDigi(bool verbose)
{
  if (fSide == 0 && fCorrTB.size() != 0)
    fTime.at(fSide) = fTime.at(fSide) - 2. * fCorrTB.at(fStrip);

  if (fTime.at(0) != 0 && fTime.at(1) != 0) {
    fTimeTB = (fTime.at(0) + fTime.at(1)) * 0.5;
    fWidthTB = (fWidth.at(0) + fWidth.at(1)) * 0.5;
    fPos.push_back((fTime.at(0) - fTime.at(1)) * 0.5 / fSignalVelosity); // X
    fPos.push_back(-4.375 + fStrip * 1.25);                              // Y
    fPos.push_back(0);                                                   // Z

    //fill histograms before cuts
    hdW_full[fStrip]->Fill(fWidth.at(0) - fWidth.at(1));
    hPos_full[fStrip]->Fill(fPos.at(0));
    hdT_full[fStrip]->Fill(fTimeTB);

    // // cat for strip amplitude correlation
    // if (TMath::Abs((fWidth.at(0) - fWidth.at(1))) > 3) {
    //   Clear();
    //   return kFALSE;
    // }

    // cat for strip position
    // if (fPos.at(0) > 3.875 || fPos.at(0) < 0.875) { // 3-3 (NINO readout)
    // if (fPos.at(0) > fPosXStripMax || fPos.at(0) < fPosXStripMin) {      // all NINO or all analogue
    //   Clear();
    //   return kFALSE;
    // }

    // cat for Time
    // if (fTimeTB < 78) {
    //   Clear();
    //   return kFALSE;
    // }

    //fill histograms after cuts
    hdW_cut[fStrip]->Fill(fWidth.at(0) - fWidth.at(1));
    hPos_cut[fStrip]->Fill(fPos.at(0));
    hdT_cut[fStrip]->Fill(fTimeTB);

    if (verbose) {
      cout << "Setting Strip#" << fStrip
           << "\nshift val " << fCorrTB.at(fStrip)
           << "\ncurr timeT " << fTime.at(0)
           << "\ncurr timeB " << fTime.at(1)
           << "\ncurr widthT " << fWidth.at(0)
           << "\ncurr widthB " << fWidth.at(1)
           << "\nfinal timeTB " << fTimeTB
           << "\nfinal widthTB " << fWidthTB
           << "\nfinal pos " << fPos.at(0) << " " << fPos.at(1) << " " << fPos.at(0) <<  "\n";
    }

    return kTRUE;
  }

  return kFALSE;
}
void AnalysisClass::GetHistParameters()
{
  TF1* squareWave = new TF1("squareWave", "[0]+[1]*(sin((x-[2])*TMath::Pi()/[3])+sin(3*(x-[2])*TMath::Pi()/[3])/3+sin(5*(x-[2])*TMath::Pi()/[3])/5+sin(7*(x-[2])*TMath::Pi()/[3])/7+sin(9*(x-[2])*TMath::Pi()/[3])/9+sin(11*(x-[2])*TMath::Pi()/[3])/11+sin(13*(x-[2])*TMath::Pi()/[3])/13+sin(15*(x-[2])*TMath::Pi()/[3])/15+sin(17*(x-[2])*TMath::Pi()/[3])/17+sin(19*(x-[2])*TMath::Pi()/[3])/19+sin(21*(x-[2])*TMath::Pi()/[3])/21+sin(23*(x-[2])*TMath::Pi()/[3])/23+sin(25*(x-[2])*TMath::Pi()/[3])/25+sin(27*(x-[2])*TMath::Pi()/[3])/27+sin(29*(x-[2])*TMath::Pi()/[3])/29+sin(31*(x-[2])*TMath::Pi()/[3])/31+sin(33*(x-[2])*TMath::Pi()/[3])/33+sin(35*(x-[2])*TMath::Pi()/[3])/35+sin(37*(x-[2])*TMath::Pi()/[3])/37+sin(39*(x-[2])*TMath::Pi()/[3])/39)");
  squareWave->SetParameters(800, 1000, 10, 10);
  // squareWave->SetParLimits(0,800,1000);
  // squareWave->SetParLimits(1,400,1300);
  squareWave->SetParLimits(2, 8, 15);
  squareWave->SetParLimits(3, 8, 15);
  for (Int_t i = 0; i < fNStr; i++) {
    TH1D* hNew = (TH1D*)hdW_full[i]->Clone("hNew");
    // hNew->Fit("squareWave", "");
    cout << "strip#" << i << " "
         // << squareWave->GetParameter(0) << " "
         // << squareWave->GetParameter(1) << " "
         // << squareWave->GetParameter(2) << " "
         // << squareWave->GetParameter(3) - squareWave->GetParameter(2) << " "
         // << squareWave->GetParameter(3) - 2 * squareWave->GetParameter(2) << endl;

         << "nEntries: " << hNew->GetEntries() << " "
         << "Int(internal): " << hNew->Integral(352, 672) << " "
         << "Int(external): " << hNew->Integral(0, 351) + hNew->Integral(673, 1024)  << endl;
  }
}

void AnalysisClass::SetTACorrection(TString filename)
{

  TFile* file = new TFile(filename);
  TObjLink* lnk = file->GetListOfKeys()->FirstLink();
  while (lnk) {
    TGraph* gr = (TGraph*)file->Get(lnk->GetObject()->GetName());
    fGraphs.push_back(gr);
    cout << lnk->GetObject()->GetName() << "\t fGraphs.size(): " << fGraphs.size() << endl;
    lnk = lnk->Next();
  }

}

double AnalysisClass::GetTACorrection(short iPlf, short iPls, double iWf, double iWs)
{
  double firstCorrection = 0;
  double secondCorrection = 0;
  for (int i = 0; i < fGraphs.size(); i++) {
    if (strstr(fGraphs.at(i)->GetName(), Form("dtPl%iPl%i", iPlf, iPls))) {
      if (strstr(fGraphs.at(i)->GetName(), "First"))
        firstCorrection += fGraphs.at(i)->Eval(iWf);
      if (strstr(fGraphs.at(i)->GetName(), "Second"))
        secondCorrection += fGraphs.at(i)->Eval(iWs);
    }
  }
  return firstCorrection + secondCorrection;
}

// void AnalysisClass::FindHit(TClonesArray* TofHit, Bool_t LRHit, pair< Double_t, Double_t > WaveTimeCorrected, pair< Double_t, Double_t > WaveAmp)
// {
//   if (LRHit) {
//     Time = (WaveTimeCorrected.first + WaveTimeCorrected.second) * 0.5;
//     Amp  = WaveAmp.first + WaveAmp.second;
//     // cout<<"\t"<<Time[it]<<"\n";
//     Pos = (WaveTimeCorrected.first - WaveTimeCorrected.second) * 0.5 / fSignalVelosity;
//     // nT0++;
//     new ((*TofHit)[TofHit->GetEntriesFast()]) DrsTofHit(kPlane, kStrip, Time, Pos, Amp);
//   }
// }
