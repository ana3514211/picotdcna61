/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   TOFContainer.h
   Author: avdmit

   Created on January 28, 2020, 10:53 AM
*/

#ifndef TOFCONTAINER_H
#define TOFCONTAINER_H

#include "TObject.h"

using namespace std;

class TOFContainer : public TObject {
public:
  /** Default constructor **/
  TOFContainer();

  /** Constructor to use **/
  TOFContainer(UShort_t iPlane, UShort_t iStrip,
               Double_t iTimeL, Double_t iTimeR,  Double_t iTime,
               Double_t iWidthL, Double_t iWidthR, Double_t iWidth);

  void SetPlane(UShort_t tmp)  { fPlane = tmp;  }
  void SetStrip(UShort_t tmp)  { fStrip = tmp;  }
  void SetTimeL(Double_t tmp)  { fTimeL = tmp;  }
  void SetTimeR(Double_t tmp)  { fTimeR = tmp;  }
  void SetTime(Double_t tmp)   { fTime = tmp;   }
  void SetWidthL(Double_t tmp) { fWidthL = tmp; }
  void SetWidthR(Double_t tmp) { fWidthR = tmp; }
  void SetWidth(Double_t tmp)  { fWidth = tmp;  }

  UShort_t GetPlane()  { return fPlane;  }
  UShort_t GetStrip()  { return fStrip;  }
  Double_t GetTimeL()  { return fTimeL;  }
  Double_t GetTimeR()  { return fTimeR;  }
  Double_t GetTime()   { return fTime;   }
  Double_t GetWidthL() { return fWidthL; }
  Double_t GetWidthR() { return fWidthR; }
  Double_t GetWidth()  { return fWidth;  }

  /** Destructor **/
  virtual ~TOFContainer() {};

private:
  UShort_t fPlane;
  UShort_t fStrip;
  Double_t fTimeL;
  Double_t fTimeR;
  Double_t fTime;
  Double_t fWidthL;
  Double_t fWidthR;
  Double_t fWidth;

  ClassDef(TOFContainer, 1);
};

#endif /* TOFCONTAINER_H */
