/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   picoHit.cpp
   Author: avdmit

   Created on January 28, 2020, 10:53 AM
*/

#include "picoHit.h"

picoHit::picoHit()
{
}

picoHit::picoHit(short iPlane, short iStrip,
                 double iTimeT, double iTimeB, double iTimeTB,
                 double iWidthT, double iWidthB, double iWidthTB,
                 double iPosX, double iPosY, double iPosZ)
{

  fPlane = iPlane;
  fStrip = iStrip;
  fTimeT = iTimeT;
  fTimeB = iTimeB;
  fTimeTB = iTimeTB;
  fPosX = iPosX;
  fPosY = iPosY;
  fPosZ = iPosZ;
  fWidthT = iWidthT;
  fWidthB = iWidthB;
  fWidthTB = iWidthTB;
  fCluster = false;

}

ClassImp(picoHit);
