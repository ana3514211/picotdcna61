/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   TOFContainer.cpp
   Author: avdmit

   Created on January 28, 2020, 10:53 AM
*/

#include "TOFContainer.h"

TOFContainer::TOFContainer()
{
}

TOFContainer::TOFContainer(UShort_t iPlane, UShort_t iStrip,
                       Double_t iTimeL, Double_t iTimeR,  Double_t iTime,
                       Double_t iWidthL, Double_t iWidthR, Double_t iWidth)
{

  fPlane = iPlane;
  fStrip = iStrip;
  fTimeL = iTimeL;
  fTimeR = iTimeR;
  fTime = iTime;
  fWidthL = iWidthL;
  fWidthR = iWidthR;
  fWidth = iWidth;

}

ClassImp(TOFContainer);
