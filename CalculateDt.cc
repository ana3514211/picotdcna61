/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   main.cpp
   Author: avdmit

   Created on November 13, 2023, 14:07 PM
*/

#include <TApplication.h>
#include <TChain.h>
#include <TClonesArray.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TH1.h>
#include <TH2.h>
#include <TGraph.h>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <unordered_map>
#include <unistd.h>
#include <chrono>
#include <thread>

#include "deltaContainer.h"

using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono; // nanoseconds, system_clock, seconds
using namespace TMath;

TString FormPat(TString nf)
{
  char currentdir[1024];
  chdir(nf.Data());
  getcwd(currentdir, sizeof(currentdir));
  TString path = Form("%s", currentdir);

  if (nf.Sizeof() == 0) {
    printf("Path to data file is \n %s \n", path.Data());
    return path;
  }
  printf("Path to data file is \n %s \n", path.Data());
  return path;
}

TString GetTimeResult(vector<double> dt, vector<double>dtErr)
{
  double dtM, dtP1, dtP2, dtE;
  cout << "MRPC/PAD1: " << dt.at(0) << "\tMRPC/PAD2: " << dt.at(1) << "\tPAD1/PAD2: " << dt.at(2) << endl;
  cout << "ErrorM/P1: " << dtErr.at(0) << "\tErrorM/P2: " << dtErr.at(1) << "\tErrorP1/P2: " << dtErr.at(2) << endl;
  dtM = Sqrt((dt.at(0) * dt.at(0) + dt.at(1) * dt.at(1) - dt.at(2) * dt.at(2)) / 2);
  dtP1 = Sqrt((dt.at(0) * dt.at(0) + dt.at(2) * dt.at(2) - dt.at(1) * dt.at(1)) / 2);
  dtP2 = Sqrt((dt.at(2) * dt.at(2) + dt.at(1) * dt.at(1) - dt.at(0) * dt.at(0)) / 2);

  dtE = Sqrt(dtErr.at(2) * dtErr.at(2) + dtErr.at(1) * dtErr.at(1) + dtErr.at(0) * dtErr.at(0));

  TString out = Form("%f\t%f\t%f\t%f", dtM, dtP1, dtP2, dtE);
  cout << "MRPC: " << dtM << "\tPAD1: " << dtP1 << "\tPAD2: " << dtP2 << endl;
  return out;
}

void Analysis(Int_t run_number, bool verbose = 0)
{

  TString name;
  short nPlanes = 3;
  vector<short> iStrip = {5, 3, 3};
  vector<double> iWidth = {0, 2., 3.};
  int nBinsX = 250;
  int fX = -1, sX = 1;
  vector <TH1D*> hnews;
  vector<double> dt;
  vector<double> dtErr;
  vector<double> dtW;
  vector<double> dtErrW;
  int pps = (iStrip.at(2) << 8) + (iStrip.at(1) << 4) + iStrip.at(0);

  std::pair<double, double> posXCutsNINO = {-3.0, -0.5};
  std::pair<double, double> posXCutsAnalogue = {2.0, 6.0};
  auto* posXCutsRef = &posXCutsAnalogue;
  if (run_number > 5)
    posXCutsRef = &posXCutsNINO;


  printf("Start analysis\n");
  TString PathToInFiles = "/home/alex/MyGitlab/picoTDC/analysis/.";
  PathToInFiles         = FormPat(PathToInFiles);
  TString FileName      = Form("%s/run%i_picoTDC_delta", PathToInFiles.Data(), run_number); // NameInFile
  FileName += ".root"; // NameInFile

  cout << "\nOpen file \n " << FileName << endl << endl;

  Int_t ResultSetBranch, ResultAdd;
  Long64_t nEvForRead;
  short nHit;

  // Read tree from file
  TChain* eveTree = new TChain("picoTDC");
  ResultAdd          = eveTree->Add(FileName, 0);
  cout << "total number of files connected = " << ResultAdd << endl;
  if (ResultAdd != 1) {
    cout << Form("Error opening files %s*\n", FileName.Data()) << endl;
    return;
  }
  nEvForRead      = eveTree->GetEntries();
  cout << "In the picoTDC file " << nEvForRead << " events \n" << endl;

  // Create outFile.root in saveFolder
  FileName = Form("%s/run%i_picoTDC", PathToInFiles.Data(), run_number); // NameOutFile
  FileName += ".dat"; // NameOutFile
  printf("Output file \n %s\n\n", FileName.Data());
  fstream fileout;
  fileout.open(FileName.Data(), ios::out);
  fileout << "mode\tdt MRPC\t\tdt PAD#1\tdt PAD#2\tdt err\t\tnEv\n";

  // Draw settings
  TF1* gaus = new TF1("gaus", "gaus");

  int nEv = 0;
  hnews.clear();
  dt.clear();
  dtErr.clear();
  dtW.clear();
  dtErrW.clear();

  for (short iPlf = 0 ; iPlf < nPlanes; ++iPlf) {
    for (short iPls = 0 ; iPls < nPlanes; ++iPls) {
      if (iPls >= iPlf)
        continue;

      short iStf = iStrip.at(iPlf);
      short iSts = iStrip.at(iPls);

      // double iPosX = 1;
      // if (iPls == 0)
      //   iPosX = -1;

      TString cutsF = Form("delta.fPlaneFirst==%i && delta.fStripFirst==%i", iPlf, iStf);
      TString cutsS = Form("delta.fPlaneSecond==%i && delta.fStripSecond==%i", iPls, iSts);
      TString cuts = Form("%s && %s &&  delta.fpps==%i", cutsF.Data(), cutsS.Data(), pps);
      // TString cutsPosX = Form("delta.fPosX>%f && delta.fPosX<%f ", posXCutsRef->first, posXCutsRef->second);
      // TString cuts = Form("%s && %s && %s && delta.fpps==%i", cutsF.Data(), cutsS.Data(), cutsPosX.Data(), pps);

      TString sHist = Form("delta.fTime>>hnew(%i,%i,%i)", nBinsX, fX, sX);

      if (verbose)
        cout << "cuts: " << cuts.Data() << endl;

      eveTree->Draw(sHist.Data(), cuts.Data(), "");
      // get hnew from the current directory
      hnews.push_back((TH1D*)gDirectory->Get("hnew"));
      nEv = hnews.back()->GetEntries();
      hnews.back()->Fit(gaus, "Q");
      dt.push_back(gaus->GetParameter(2) * 1000);
      dtErr.push_back(gaus->GetParError(2) * 1000);
      sleep_until(system_clock::now() + milliseconds(300));

      hnews.back()->Fit(gaus, "WQ");
      dtW.push_back(gaus->GetParameter(2) * 1000);
      dtErrW.push_back(gaus->GetParError(2) * 1000);

      // TString amplitudeCut = Form("delta.fWidthFirst>%i && delta.fWidthSecond>%i", iWidth.at(iPlf), iWidth.at(iPls));
      // TString extra_cuts = Form("%s && %s", cuts.Data(), amplitudeCut.Data());
      // eveTree->Draw(sHist.Data(), extra_cuts.Data(), "");
      // TH1D* hnew = (TH1D*)gDirectory->Get("hnew");
      // hnew->Fit(gaus, "Q");
      // nEv =hnew
    }
  }

  // if (verbose) {
  //   TCanvas* c = new TCanvas("c", "The Canvas", 0, 0, 1440, 1000);
  //   c->Clear("D");
  //   TPad* smallpad = new TPad("smallpad", "", 0.0, 0.0, 1, 1);
  //   smallpad->SetFillStyle(4000);
  //   smallpad->Draw();
  //   smallpad->Divide(3, 1, 0.2, 0);
  //   for (short iC = 0 ; iC < hnews.size(); ++iC) {
  //     smallpad->cd(iC + 1);
  //     smallpad->SetTickx(1);
  //     hnews.at(iC)->Draw();
  //     smallpad->Update();
  //   }
  //   getchar();
  // }
  //
  cout << "\nnEv: " << nEv << endl;

  cout << "\nempty settings" << endl;
  TString results = GetTimeResult(dt, dtErr);
  fileout << "empty\t" << results.Data() << "\t" << nEv << "\n";
  cout << "\nweight settings" << endl;
  results = GetTimeResult(dtW, dtErrW);
  fileout << "weight\t" << results.Data() << "\t" << nEv << "\n";

  fileout.close();
}

int main(int argc, char** argv)
{

  Int_t run_number;
  bool verbose = 0;
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] <<
              " <run number> <verboseMode, default is 0>, <version, default is 0>" << std::endl;
    return 1;
  }
  else if (argc >= 2)
    run_number = atoi(argv[1]);
  if (argc > 2)
    verbose  = atoi(argv[2]);
  std::cout << "Doing analysis for run " << run_number << "\n";
  TApplication theApp("App", &argc, argv);
  Analysis(run_number, verbose);
  std::cout << "Analysis complete!" << std::endl;
  return 0;

} // end int main
