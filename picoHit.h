/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   picoHit.h
   Author: avdmit

   Created on January 28, 2020, 10:53 AM
*/

#ifndef PICOHIT_H
#define PICOHIT_H

#include "TObject.h"

using namespace std;

class picoHit : public TObject {
public:
  /** Default constructor **/
  picoHit();

  /** Constructor to use **/
  picoHit(short iPlane, short iStrip,
          double iTimeT, double iTimeB, double iTimeTB,
          double iWidthT, double iWidthB, double iWidthTB,
          double iPosX, double iPosY, double iPosZ);

  void SetPlane(short tmp)   {  fPlane = tmp;   }
  void SetStrip(short tmp)   {  fStrip = tmp;   }
  void SetTimeT(double tmp)  {  fTimeT = tmp;   }
  void SetTimeB(double tmp)  {  fTimeB = tmp;   }
  void SetTime(double tmp)   {  fTimeTB = tmp;  }
  void SetPosX(double tmp)   {  fPosX = tmp;    }
  void SetPosY(double tmp)   {  fPosY = tmp;    }
  void SetPosZ(double tmp)   {  fPosZ = tmp;    }
  void SetWidthT(double tmp) {  fWidthT = tmp;  }
  void SetWidthB(double tmp) {  fWidthB = tmp;  }
  void SetWidth(double tmp)  {  fWidthTB = tmp; }
  void SetCluster(bool tmp)  {  fCluster = tmp; }

  short GetPlane()   {   return fPlane;   }
  short GetStrip()   {   return fStrip;   }
  double GetTimeT()  {   return fTimeT;   }
  double GetTimeB()  {   return fTimeB;   }
  double GetTime()   {   return fTimeTB;  }
  double GetPosX()   {   return fPosX;    }
  double GetPosY()   {   return fPosY;    }
  double GetPosZ()   {   return fPosZ;    }
  double GetWidthT() {   return fWidthT;  }
  double GetWidthB() {   return fWidthB;  }
  double GetWidth()  {   return fWidthTB; }
  double GetCluster(){   return fCluster; }

  /** Destructor **/
  virtual ~picoHit() {};

private:
  short fPlane;
  short fStrip;
  double fTimeT;
  double fTimeB;
  double fTimeTB;
  double fPosX;
  double fPosY;
  double fPosZ;
  double fWidthT;
  double fWidthB;
  double fWidthTB;
  bool fCluster;

  ClassDef(picoHit, 1);
};

#endif /* PICOHIT_H */
