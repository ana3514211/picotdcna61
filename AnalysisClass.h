/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   AnalysisClass.h
   Author: avdmit

   Created on January 07, 2020, 10:05 AM
*/
#include <iostream>
#include <vector>
#include <TMath.h>
#include <TH1.h>
#include <TF1.h>
#include <TFile.h>
#include <TGraph.h>
#include <TString.h>
#include <TClonesArray.h>

#include "picoDigit.h"
#include "picoHit.h"

using namespace std;

class AnalysisClass {
public:
  AnalysisClass();
  // Standart functions
  Bool_t pushDigi(picoDigit*, bool);
  void findHit(TClonesArray*);
  void Clear();
  void Init(int run_number);
  void GetHistParameters();
  void SetTACorrection(TString);
  double GetTACorrection(short, short, double, double);

  TList* GetHistlist(short i) { return fHistLists.at(i); }

  double GetTimeTop()  { return fTime.at(0);  }
  double GetTimeBot()  { return fTime.at(1);  }
  double GetTime()     { return fTimeTB;      }
  double GetWidthTop() { return fWidth.at(0); }
  double GetWidthBot() { return fWidth.at(1); }
  double GetWidth()    { return fWidthTB;     }
  double GetPosX()     { return fPos.at(0);   }
  double GetPosY()     { return fPos.at(1);   }
  double GetPosZ()     { return fPos.at(2);   }

private:
  // Standart varalibles
  static const short fNStr = 8;
  Short_t fPlane, fStrip, fSide;
  double fTimeTB, fWidthTB, fPosXStripMax, fPosXStripMin;
  vector<double> fTime;
  vector<double> fWidth;
  vector<double> fCorrTB;
  vector<double> fPos;
  vector<double> fZ;

  const Double_t fSignalVelosity = 0.06; // 0.06 ns/cm
  const Short_t fStripLength = 15; // cm
  const Double_t fMaxDelta = fStripLength * 0.5 + 2.0; // + 20 mm on the strip edge

  vector<TList*> fHistLists;

  vector<TGraph*> fGraphs;

  TH1D* hdW_full[fNStr];
  TH1D* hdW_cut[fNStr];
  TH1D* hPos_full[fNStr];
  TH1D* hPos_cut[fNStr];
  TH1D* hdT_full[fNStr];
  TH1D* hdT_cut[fNStr];

  // Standart functions
  // void FindHit(TClonesArray* TofHit, Bool_t, pair< Double_t, Double_t >, pair< Double_t, Double_t >);
  Bool_t setPadDigi(bool);
  Bool_t setStripDigi(bool);
};
