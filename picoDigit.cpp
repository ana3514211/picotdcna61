/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   picoDigit.cpp
   Author: avdmit

   Created on January 28, 2020, 10:53 AM
*/

#include "picoDigit.h"

picoDigit::picoDigit()
{
}

picoDigit::picoDigit(short iPlane, short iStrip, short iSide,
                     double iTime, double iWidth)
{

  fPlane = iPlane;
  fStrip = iStrip;
  fSide  = iSide;
  fTime  = iTime;
  fWidth = iWidth;

}

ClassImp(picoDigit);
