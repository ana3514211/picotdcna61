/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   main.cpp
   Author: avdmit

   Created on October 25, 2023, 07:12 PM
*/

#include <TApplication.h>
#include <TChain.h>
#include <TClonesArray.h>
#include <TFile.h>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <unistd.h>
#include <unordered_map>

// #include "event.h"
#include "picoDigit.h"
// #include "analysisClass.h"

using namespace std;

struct FileHeader {
  int8_t dataFormatVersion[2];
  int8_t softwareVersion[3];
  int16_t versionFERS;
  int16_t runNumber;
  int16_t acqMode;
  int8_t measMode;
  int8_t timeUnit;
  float toaLSBvalue;
  float totLSBvalue;
  float timestampLSBvalue;
  int64_t startRun; // in ms since epoch
};

struct TriggerHeader {
  int16_t eventSize;
  double triggerTime;
  int64_t triggerId;
  int16_t hitNumber;
};

struct Event {
  int8_t boardId;
  int8_t channelId;
  float ToA;
  float ToT;
};

struct ANAEvent {
  short side;
  float ToA;
  float ToT;
};

std::unordered_map <short, std::pair<short, short>> channelMapAnalogue = {
  {32, std::make_pair(0, 7)},  {33, std::make_pair(0, 6)},  {34, std::make_pair(0, 5)},  {35, std::make_pair(0, 4)},
  {36, std::make_pair(0, 3)},  {37, std::make_pair(0, 2)},  {38, std::make_pair(0, 1)},  {39, std::make_pair(0, 0)}, //MRPC
  {40, std::make_pair(0, 0)},  {41, std::make_pair(0, 1)},  {42, std::make_pair(0, 2)},  {43, std::make_pair(0, 3)},
  {44, std::make_pair(0, 4)},  {45, std::make_pair(0, 5)},  {46, std::make_pair(0, 6)},  {47, std::make_pair(0, 7)}, //MRPC
  {16, std::make_pair(1, 9)},  {17, std::make_pair(1, 8)},  {18, std::make_pair(1, 11)}, {19, std::make_pair(1, 10)},
  {20, std::make_pair(1, 13)}, {21, std::make_pair(1, 12)}, {22, std::make_pair(1, 15)}, {23, std::make_pair(1, 14)}, //PAD1
  {24, std::make_pair(1, 1)},  {25, std::make_pair(1, 0)},  {26, std::make_pair(1, 3)},  {27, std::make_pair(1, 2)},
  {28, std::make_pair(1, 5)},  {29, std::make_pair(1, 4)},  {30, std::make_pair(1, 7)},  {31, std::make_pair(1, 6)}, //PAD1
  {48, std::make_pair(2, 14)}, {49, std::make_pair(2, 15)}, {50, std::make_pair(2, 12)}, {51, std::make_pair(2, 13)},
  {52, std::make_pair(2, 10)}, {53, std::make_pair(2, 11)}, {54, std::make_pair(2, 8)},  {55, std::make_pair(2, 9)}, //PAD2
  {56, std::make_pair(2, 6)},  {57, std::make_pair(2, 7)},  {58, std::make_pair(2, 4)},  {59, std::make_pair(2, 5)},
  {60, std::make_pair(2, 2)},  {61, std::make_pair(2, 3)},  {62, std::make_pair(2, 0)},  {63, std::make_pair(2, 1)}, //PAD2
};

std::unordered_map <short, std::pair<short, short>> channelMapNINO = {
  {32, std::make_pair(0, 7)},  {33, std::make_pair(0, 6)},  {34, std::make_pair(0, 5)},  {35, std::make_pair(0, 4)},
  {36, std::make_pair(0, 3)},  {37, std::make_pair(0, 2)},  {38, std::make_pair(0, 1)},  {39, std::make_pair(0, 0)}, //MRPC
  {40, std::make_pair(0, 0)},  {41, std::make_pair(0, 1)},  {42, std::make_pair(0, 2)},  {43, std::make_pair(0, 3)},
  {44, std::make_pair(0, 4)},  {45, std::make_pair(0, 5)},  {46, std::make_pair(0, 6)},  {47, std::make_pair(0, 7)}, //MRPC
  {16, std::make_pair(1, 8)},  {17, std::make_pair(1, 9)},  {18, std::make_pair(1, 10)}, {19, std::make_pair(1, 11)},
  {20, std::make_pair(1, 12)}, {21, std::make_pair(1, 13)}, {22, std::make_pair(1, 14)}, {23, std::make_pair(1, 15)}, //PAD1
  {24, std::make_pair(1, 0)},  {25, std::make_pair(1, 1)},  {26, std::make_pair(1, 2)},  {27, std::make_pair(1, 3)},
  {28, std::make_pair(1, 4)},  {29, std::make_pair(1, 5)},  {30, std::make_pair(1, 6)},  {31, std::make_pair(1, 7)}, //PAD1
  {48, std::make_pair(2, 15)}, {49, std::make_pair(2, 14)}, {50, std::make_pair(2, 13)}, {51, std::make_pair(2, 12)},
  {52, std::make_pair(2, 11)}, {53, std::make_pair(2, 10)}, {54, std::make_pair(2, 9)},  {55, std::make_pair(2, 8)}, //PAD2
  {56, std::make_pair(2, 7)},  {57, std::make_pair(2, 6)},  {58, std::make_pair(2, 5)},  {59, std::make_pair(2, 4)},
  {60, std::make_pair(2, 3)},  {61, std::make_pair(2, 2)},  {62, std::make_pair(2, 1)},  {63, std::make_pair(2, 0)}, //PAD2
};

short GetSide(short channel)
{
  if (channel >= 32 && channel <= 39)
    return 1;
  return 0;
}

TString GetDetectorName(int8_t channel)
{
  if (channel == 0)
    return "\tTrigger";
  if (channel > 15 && channel < 32)
    return "\tPad#1";
  if (channel > 31 && channel < 48)
    return "\tStrip";
  if (channel > 47)
    return "\tPad#2";

}

TString FormPat(TString nf)
{
  char currentdir[1024];
  chdir(nf.Data());
  getcwd(currentdir, sizeof(currentdir));
  TString path = Form("%s", currentdir);

  if (nf.Sizeof() == 0) {
    printf("Path to data file is \n %s \n", path.Data());
    return path;
  }
  printf("Path to data file is \n %s \n", path.Data());
  return path;
}

void Analysis(Int_t run_number, bool verbose = 0)
{

  TString name;

  TList* histlist = new TList();

  printf("Start analysis\n");
  TString PathToInFiles = "/home/alex/MyGitlab/picoTDC/.";
  TString saveFolder    = "analysis";
  PathToInFiles         = FormPat(PathToInFiles);
  TString FileName      = Form("%s/data/Run%i_list", PathToInFiles.Data(), run_number); // NameInFile
  FileName += ".dat"; // NameInFile
  auto* channelMapRef = &channelMapAnalogue;
  if (run_number > 5)
    channelMapRef = &channelMapNINO;
  // Create saveFolder if it's not exist
  struct stat statbuf;
  if (!stat(Form("%s/%s", PathToInFiles.Data(), saveFolder.Data()), &statbuf) != -1) {
    if (!S_ISDIR(statbuf.st_mode)) {
      system(Form("mkdir -p %s/%s", PathToInFiles.Data(), saveFolder.Data()));
    }
  }
  cout << "\nOpen file \n " << FileName << endl << endl;
  FileHeader hdr;
  TriggerHeader trigger;
  Event event;
  unsigned int nEv = 0;

  ifstream in;
  in.open(FileName.Data(), std::fstream::in | std::fstream::binary);
  if (!in.is_open()) {
    cout << "[ERROR] Can't open the file: " << FileName.Data()  << endl;
    return;
  }
  cout << "File successfully open" << endl;
  in.read((char*)&hdr.dataFormatVersion, sizeof(hdr.dataFormatVersion));
  in.read((char*)&hdr.softwareVersion, sizeof(hdr.softwareVersion));
  in.read((char*)&hdr.versionFERS, sizeof(hdr.versionFERS));
  in.read((char*)&hdr.runNumber, sizeof(hdr.runNumber));
  in.read((char*)&hdr.acqMode, sizeof(hdr.acqMode));
  in.read((char*)&hdr.measMode, sizeof(hdr.measMode));
  in.read((char*)&hdr.timeUnit, sizeof(hdr.timeUnit));
  in.read((char*)&hdr.toaLSBvalue, sizeof(hdr.toaLSBvalue));
  in.read((char*)&hdr.totLSBvalue, sizeof(hdr.totLSBvalue));
  in.read((char*)&hdr.timestampLSBvalue, sizeof(hdr.timestampLSBvalue));
  in.read((char*)&hdr.startRun, sizeof(hdr.startRun));
  const auto p0 = std::chrono::time_point<std::chrono::system_clock> {} + std::chrono::milliseconds(hdr.startRun);
  std::time_t run_time = std::chrono::system_clock::to_time_t(p0);
  auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(p0.time_since_epoch()).count() % 1000;
  struct tm* t = localtime(&run_time);
  cout << "====================== FILE HEDER ======================" << endl;
  cout << "dataFormat:\t\t" << static_cast<int16_t>(hdr.dataFormatVersion[0]) << "." << static_cast<int16_t>(hdr.dataFormatVersion[1]) << "\n"
       << "software:\t\t" << static_cast<int16_t>(hdr.softwareVersion[0]) << "." << static_cast<int16_t>(hdr.softwareVersion[1]) << "." << static_cast<int16_t>(hdr.softwareVersion[2]) << "\n"
       << "FERS:\t\t\t" << hdr.versionFERS << "\n"
       << "runNumber:\t\t" << hdr.runNumber << "\n"
       << "acquisitionMode:\t" << hdr.acqMode << "\t(2 for Common Start; 18 for Common Stop; 34 for Streaming; 50 for Trigger Matching; 1 Test Mode)\n"
       << "measurementMode:\t" << static_cast<int16_t>(hdr.measMode) << "\t(1 for LEAD_ONLY; 3 for LEAD_TRAIL; 5 for LEAD_TOT8; 9 for LEAD_TOT11)\n"
       << "timeUnit:\t\t" << static_cast<int16_t>(hdr.timeUnit) << "\t(0 in LSB; 1 in ns)\n"
       << "ToALSBvalue, ps:\t" << hdr.toaLSBvalue << "\n"
       << "ToTLSBvalue, ps:\t" << hdr.totLSBvalue << "\n"
       << "timestampLSBvalue, ps:\t" << hdr.timestampLSBvalue << "\n"
       << "startRun:\t\t" << Form("%04d-%02d-%02d  %02d:%02d:%02d.%03d\n",
                                  t->tm_year + 1900, t->tm_mon + 1, t->tm_mday,
                                  t->tm_hour, t->tm_min, t->tm_sec, ms)
       << endl << endl;

  // Create outFile.root in saveFolder
  FileName = Form("%s/%s/run%i_picoTDC", PathToInFiles.Data(), saveFolder.Data(), run_number); // NameOutFile
  FileName += ".root"; // NameOutFile
  printf("Output file \n %s\n\n", FileName.Data());
  TFile* FileOut = new TFile(FileName, "RECREATE");
  TTree* t_out   = new TTree("picoTDC", "picoTDC");

  TClonesArray* OutArray = new TClonesArray("picoDigit");
  float triggerTime;
  std::time_t event_time;
  int event_time_ms;
  t_out->Branch("picoDigits", &OutArray);
  t_out->Branch("triggerTime", &triggerTime);
  t_out->Branch("eventTime", &event_time);
  t_out->Branch("eventTimeMs", &event_time_ms);

  while (!in.eof()) {
    OutArray->Clear();

    ++nEv;
    if (!(nEv % 50000))
      cout << nEv / 1000 << "K Events have been read" << endl;

    //read event header
    in.read((char*)&trigger.eventSize, sizeof(trigger.eventSize));
    in.read((char*)&trigger.triggerTime, sizeof(trigger.triggerTime));
    in.read((char*)&trigger.triggerId, sizeof(trigger.triggerId));
    in.read((char*)&trigger.hitNumber, sizeof(trigger.hitNumber));
    const auto p1 = p0 + std::chrono::nanoseconds((long)(trigger.triggerTime * 1000));
    event_time = std::chrono::system_clock::to_time_t(p1);
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(p1.time_since_epoch()).count() % 1000;
    event_time_ms = ms;
    struct tm* t = localtime(&event_time);
    if (verbose) {
      cout << "====================== EVENT HEDER ======================" << endl;
      cout << "eventSize, bytes:\t" << trigger.eventSize << "\n"
           << "triggerTime:\t\t" << Form("%04d-%02d-%02d  %02d:%02d:%02d.%03d\n",
                                         t->tm_year + 1900, t->tm_mon + 1, t->tm_mday,
                                         t->tm_hour, t->tm_min, t->tm_sec, ms)
           << "triggerId:\t\t" << trigger.triggerId << "\n"
           << "hitNumber:\t\t" << trigger.hitNumber << "\n";
    }

    bool missTrigger = false;
    std::unordered_map <short, std::unordered_map<short, ANAEvent>> anaEvents;
    for (int hits = 0; hits < trigger.hitNumber; ++hits) {
      in.read((char*)&event.boardId, sizeof(event.boardId));
      in.read((char*)&event.channelId, sizeof(event.channelId));
      in.read((char*)&event.ToA, sizeof(event.ToA));
      in.read((char*)&event.ToT, sizeof(event.ToT));

      // If trigger channel is not presented skip all events
      if (hits == 0 && event.channelId != 0)
        missTrigger = true;

      // Skip events with ToT = 0 or ToT > 200
      if (hits != 0 && (event.ToT == 0 || event.ToT > 200))
        continue;

      if (verbose) {
        cout << "====================== HIT " << hits  << " ======================" << endl;
        cout << "boardId:\t\t" << static_cast<int16_t>(event.boardId) << "\n"
             << "channelId:\t\t" << static_cast<int16_t>(event.channelId) << GetDetectorName(event.channelId) << "\n"
             << "ToA, ns:\t\t" << event.ToA << "\n"
             << "ToT, ns:\t\t" << event.ToT << endl;

        if (missTrigger)
          cout << "Trigger is missed" << endl;
      }

      if (missTrigger)
        continue;

      if (event.channelId != 0) {
        auto planeId = channelMapRef->at(event.channelId).first;
        auto stripId = channelMapRef->at(event.channelId).second;
        auto side = GetSide(event.channelId);

        new ((*OutArray)[OutArray->GetEntriesFast()]) picoDigit(planeId, stripId,
            side, event.ToA, event.ToT);

      } // end of if channel not trigger one
      else
        triggerTime = event.ToA;
    } // end of loop over hits

    if (verbose)
      getchar();

    if (OutArray->GetEntriesFast() != 0)
      t_out->Fill();
  }

  FileOut->cd();
  t_out->Print();
  t_out->Write();
  histlist->Write();
  FileOut->Close();

  cout << "TOTAL:\t" << nEv << " Events have been read" << endl;
  // cout << "TOTAL:\t" << nGoodEv << " good events " << ((double) nGoodEv / nEv) * 100 << " have been read" << endl;
  in.close();
}

int main(int argc, char** argv)
{

  Int_t run_number;
  bool verbose = 0;
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " <run number> <verboseMode, default is 0>" << std::endl;
    return 1;
  }
  else if (argc >= 2)
    run_number = atoi(argv[1]);
  if (argc > 2)
    verbose  = atoi(argv[2]);
  std::cout << "Doing analysis for run " << run_number << "\n";
  TApplication theApp("App", &argc, argv);
  Analysis(run_number, verbose);
  std::cout << "Analysis complete!" << std::endl;
  return 0;

} // end int main
