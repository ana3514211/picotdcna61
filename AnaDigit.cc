/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   main.cpp
   Author: avdmit

   Created on November 13, 2023, 14:07 PM
*/

#include <TApplication.h>
#include <TChain.h>
#include <TClonesArray.h>
#include <TFile.h>
#include <TH1.h>
#include <TSystem.h>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <unordered_map>
#include <unistd.h>

#include "picoDigit.h"
#include "picoHit.h"
#include "deltaContainer.h"
#include "AnalysisClass.h"

using namespace std;

TString FormPat(TString nf)
{
  char currentdir[1024];
  chdir(nf.Data());
  getcwd(currentdir, sizeof(currentdir));
  TString path = Form("%s", currentdir);

  if (nf.Sizeof() == 0) {
    printf("Path to data file is \n %s \n", path.Data());
    return path;
  }
  printf("Path to data file is \n %s \n", path.Data());
  return path;
}

bool checkCluster(int idigSet,  TClonesArray** hitArray)
{
  double maxDistance = sqrt(1.75 * 1.75 + 3.5 * 3.5); // circle
  // double maxDistance = 3.5; // cross
  // double maxDistance = 1.75; // up/down
  // cout << "checking cluster" << endl;

  picoHit* digSet = (picoHit*)(*hitArray)->At(idigSet);
  int planeSet = digSet->GetPlane();
  for (Int_t idig = 0; idig < (*hitArray)->GetEntriesFast(); ++idig) {
    picoHit* dig = (picoHit*)(*hitArray)->At(idig);
    if (dig != digSet)
      if (dig->GetPlane() == planeSet) {
        double dX = abs(dig->GetPosX() - digSet->GetPosX());
        double dY = abs(dig->GetPosY() - digSet->GetPosY());
        double distance = sqrt(dX * dX + dY * dY);
        // cout << "distance: " << distance << endl;
        if (distance <= maxDistance)
          return true;
      }
  }
  return false;
}

void Analysis(Int_t run_number, bool verbose = 0)
{

  TString name;
  short nPlanes = 3;
  vector<short> nStrips = {8, 16, 16};
  vector<short> iStrip = {5, 3, 3};
  vector<short> nSides = {2, 1, 1};
  bool neighbourhood = false;

  TList* histlist[nPlanes];
  TH1S* pads[nPlanes - 1][16];

  for (Int_t i = 1; i < nPlanes; i++) {
    histlist[i] = new TList();
    for (short iSt = 0; iSt < nStrips.at(i); ++iSt) {
      pads[i - 1][iSt] = new TH1S(Form("pads_plane_%d_strip_%d", i, iSt),
                                  Form("pads_plane_%d_strip_%d", i, iSt),
                                  16, 0, 16);
      histlist[i]->Add(pads[i - 1][iSt]);
    }
  }

  printf("Start analysis\n");
  TString PathToInFiles = "/home/alex/MyGitlab/picoTDC/analysis/.";
  PathToInFiles         = FormPat(PathToInFiles);
  TString FileName      = Form("%s/run%i_picoTDC", PathToInFiles.Data(), run_number); // NameInFile
  FileName += ".root"; // NameInFile

  cout << "\nOpen file \n " << FileName << endl << endl;

  TClonesArray* picoDigits = new TClonesArray("picoDigit");
  Int_t ResultSetBranch, ResultAdd;
  Long64_t nEvForRead;
  float triggerTime;
  time_t event_time;
  int event_time_ms;
  AnalysisClass ana;
  ana.Init(run_number);
  // Read tree from file
  TChain* eveTree = new TChain("picoTDC");
  ResultAdd          = eveTree->Add(FileName, 0);
  cout << "total number of files connected = " << ResultAdd << endl;
  if (ResultAdd != 1) {
    cout << Form("Error opening files %s*\n", FileName.Data()) << endl;
    return;
  }
  ResultSetBranch = eveTree->SetBranchAddress("picoDigits", &picoDigits);
  printf("picoDigits Set Branch = %i\n", ResultSetBranch);
  ResultSetBranch = -1;
  ResultSetBranch = eveTree->SetBranchAddress("triggerTime", &triggerTime);
  printf("triggerTime Set Branch = %i\n", ResultSetBranch);
  ResultSetBranch = -1;
  ResultSetBranch = eveTree->SetBranchAddress("eventTime", &event_time);
  printf("eventTime Set Branch = %i\n", ResultSetBranch);
  ResultSetBranch = -1;
  ResultSetBranch = eveTree->SetBranchAddress("eventTimeMs", &event_time_ms);
  printf("eventTimeMs Set Branch = %i\n", ResultSetBranch);
  ResultSetBranch = -1;
  nEvForRead      = eveTree->GetEntries();
  cout << "In the picoTDC file " << nEvForRead << " events \n" << endl;
  // nEvForRead      = (750000 > nEvForRead) ? nEvForRead : 750000;
  cout << "Will be read " << nEvForRead << " events \n" << endl;

  //Set TA correction
  short version = 0;
  TString amplitude = "First";
  FileName = Form("%s/run%i_picoTDC_TA_%s_v%i", PathToInFiles.Data(), run_number, amplitude.Data(), version); // NameInFile
  FileName += ".root"; // NameInFile

  while (!gSystem->AccessPathName(FileName)) {
    cout << "Setting TA correction from: " << FileName.Data() << endl;
    ana.SetTACorrection(FileName);

    amplitude = "Second";
    FileName = Form("%s/run%i_picoTDC_TA_%s_v%i", PathToInFiles.Data(), run_number, amplitude.Data(), version); // NameInFile
    FileName += ".root"; // NameInFile
    if (!gSystem->AccessPathName(FileName)) {
      cout << "Setting TA correction from: " << FileName.Data() << endl;
      ana.SetTACorrection(FileName);
      cout << "\n" ;
    }
    ++version;
    amplitude = "First";
    FileName = Form("%s/run%i_picoTDC_TA_%s_v%i", PathToInFiles.Data(), run_number, amplitude.Data(), version); // NameInFile
    FileName += ".root"; // NameInFile
  }

  // Create outFile.root in saveFolder
  FileName = Form("%s/run%i_picoTDC_ana", PathToInFiles.Data(), run_number); // NameOutFile
  FileName += ".root"; // NameOutFile
  printf("Output file \n %s\n\n", FileName.Data());
  TFile* fileout = new TFile(FileName, "RECREATE");
  TTree* t_out   = new TTree("picoTDC", "picoTDC");

  vector<TClonesArray*> hitArrays = {new TClonesArray("picoHit"),
                                     new TClonesArray("picoHit"),
                                     new TClonesArray("picoHit")
                                    };
  TClonesArray* dtArray = new TClonesArray("deltaContainer");
  TClonesArray* dtStripArray = new TClonesArray("deltaContainer");
  short nHit;
  for (short iPl = 0 ; iPl < nPlanes; ++iPl)
    t_out->Branch(Form("picoHits%i", iPl), &hitArrays.at(iPl));
  t_out->Branch("delta", &dtArray);
  if (neighbourhood)
    t_out->Branch("deltaS", &dtStripArray);
  t_out->Branch("nHits", &nHit);

  // TClonesArray* hitArray = new TClonesArray("Ana");

  for (Int_t iEv = 0; iEv < /* 5000;  */ nEvForRead; iEv++) {
    picoDigits->Clear();
    dtArray->Clear();
    dtStripArray->Clear();
    for (short iPl = 0 ; iPl < nPlanes; ++iPl)
      hitArrays.at(iPl)->Clear();

    eveTree->GetEntry(iEv);
    if (iEv % 50000 == 0)
      cout << iEv / 1000 << "K Events have been read" << endl;

    if (triggerTime < 210)
      continue;

    if (verbose)
      cout << "====================== EVENT " << iEv  << " ======================" << endl;
    nHit = 0;
    for (short iPl = nPlanes - 1 ; iPl > -1; --iPl) {
      bool isHit = false;
      for (short iSt = 0; iSt < nStrips.at(iPl); ++iSt) {
        // short iSt = iStrip.at(iPl);
        ana.Clear();

        for (short iSd = 0; iSd < nSides.at(iPl); ++iSd) {
          for (Int_t idig = 0; idig < picoDigits->GetEntriesFast(); ++idig) {
            picoDigit* dig = (picoDigit*) picoDigits->At(idig);

            if (dig->GetPlane() != iPl)
              continue;
            if (dig->GetStrip() != iSt)
              continue;
            if (dig->GetSide() != iSd)
              continue;

            // if (iPl == 2)
            //     if (iSt != 4)
            //       continue;
            // if (iPl == 1)
            //     if (iSt != 4)
            //       continue;

            if (ana.pushDigi(dig, verbose)) {
              isHit = true;

              // if (!hitArray->IsEmpty()) {
              //   for (Int_t ihit = 0; ihit < hitArray->GetEntriesFast(); ++ihit) {
              //     picoHit* hit = (picoHit*) hitArray->At(ihit);
              //     // if (hit->GetPlane() == iPl)
              //       // continue;
              //     new ((*dtArray)[dtArray->GetEntriesFast()]) deltaContainer(iPl, hit->GetPlane(),
              //         iSt, hit->GetStrip(),
              //         ana.GetWidth(), hit->GetWidth(),
              //         ana.GetTime() - hit->GetTime() - ana.GetTACorrection(iPl, hit->GetPlane(),
              //             ana.GetWidth(), hit->GetWidth()),
              //         ana.GetPosX() -  hit->GetPosX(),
              //         ana.GetPosY() - hit->GetPosY());
              //   }
              // }
              new ((*hitArrays.at(iPl))[hitArrays.at(iPl)->GetEntriesFast()]) picoHit(iPl, iSt,
                  ana.GetTimeTop(), ana.GetTimeBot(), ana.GetTime(),
                  ana.GetWidthTop(), ana.GetWidthBot(), ana.GetWidth(),
                  ana.GetPosX(), ana.GetPosY(), ana.GetPosZ());
            }
            // if (iSt == 5) {
            //   isStripSelected = true;
            //   if (isCompiled)
            //     ++nHit;
            // }
            // if (verbose && isCompiled)
            //   cout << "strip/pad compiled" << "\n" << endl;
            // }
            // else {
            //   if (isStripSelected)
            //     pads[iPl - 1][iSt]->Fill(0);
            // }

            // if (verbose) {
            //   cout << "#" << dig->GetPlane()
            //        << " St:" << dig->GetStrip()
            //        << " Si:" << dig->GetSide() << "\n"
            //        << dig->GetTime() << "\t" << dig->GetWidth()  << "\n" << endl;
            // }

          } // end of loop over digits
        } //end of loop over sides
      } //end of loop over strips
      nHit = (isHit) ? nHit + 1 : nHit;
      if (verbose)
        cout << "-----------------------\n" << endl;

      //Cluster finder
      for (int ihit = 0; ihit < hitArrays.at(iPl)->GetEntriesFast(); ++ihit) {
        picoHit* hit = (picoHit*) hitArrays.at(iPl)->At(ihit);
        if (hit->GetPlane() == iPl) {
          bool cluster = checkCluster(ihit, &hitArrays.at(iPl));
          hit->SetCluster(cluster);
        }
      }

    } //end of loop over planes
    if (verbose && nHit == 3)
      getchar();

    // if (!hitArray->IsEmpty() && neighbourhood) {
    //   for (Int_t ihit1 = 0; ihit1 < hitArray->GetEntriesFast(); ++ihit1) {
    //     picoHit* hit1 = (picoHit*) hitArray->At(ihit1);

    //     for (Int_t ihit2 = 0; ihit2 < hitArray->GetEntriesFast(); ++ihit2) {
    //       picoHit* hit2 = (picoHit*) hitArray->At(ihit2);

    //       if (ihit1 == ihit2)
    //         continue;
    //       new ((*dtStripArray)[dtStripArray->GetEntriesFast()]) deltaContainer(hit1->GetPlane(), hit2->GetPlane(),
    //           hit1->GetStrip(), hit2->GetStrip(),
    //           hit1->GetWidth(), hit2->GetWidth(),
    //           hit1->GetTime() - hit2->GetTime(),
    //           hit1->GetPosX() - hit2->GetPosX(),
    //           hit1->GetPosY() - hit2->GetPosY());
    //     } // end of ihit2 loop
    //   } // end of ihit1 loop
    // } // end of neighbourhood fill

    t_out->Fill();
  } //end of loop over events

  // ana.GetHistParameters();

  for (short iPl = 0; iPl < nPlanes; ++iPl) {
    TDirectory* dir;
    TString name;
    name = Form("Plane#%d", iPl);
    dir = fileout->mkdir(name.Data());
    dir->cd();

    if (iPl == 0) {
      t_out->Draw("picoHits0@.GetEntries()>>hnew(8,-0.5,7.5)",
                  "picoHits1.fStrip==3&&picoHits2.fStrip==3&&!picoHits1.fCluster&&!picoHits2.fCluster&&nHits==3",
                  "");
      // get hnew from the current directory
      TH1I* hnew = (TH1I*)gDirectory->Get("hnew");
      Double_t factor = 1.;
      hnew->Scale(factor / hnew->GetEntries());
      hnew->Write();

      for (short iSt = 0; iSt < nStrips.at(iPl); ++iSt) {
        TDirectory* dirS;
        name = Form("Strip#%d", iSt);
        dirS = dir->mkdir(name.Data());
        dirS->cd();

        histlist[iPl] = ana.GetHistlist(iSt);
        histlist[iPl]->Write();
      }
    }
    else
      histlist[iPl]->Write();
  }
  fileout->cd();
  // t_out->Print();
  t_out->Write();
  fileout->Close();
}

int main(int argc, char** argv)
{

  Int_t run_number;
  bool verbose = 0;
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " <run number> <verboseMode, default is 0>" << std::endl;
    return 1;
  }
  else if (argc >= 2)
    run_number = atoi(argv[1]);
  if (argc > 2)
    verbose  = atoi(argv[2]);
  std::cout << "Doing analysis for run " << run_number << "\n";
  TApplication theApp("App", &argc, argv);
  Analysis(run_number, verbose);
  std::cout << "Analysis complete!" << std::endl;
  return 0;

} // end int main
