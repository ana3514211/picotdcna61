// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME Dict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "ROOT/RConfig.hxx"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Header files passed as explicit arguments
#include "AnalysisClass.h"
#include "deltaContainer.h"
#include "picoDigit.h"
#include "picoHit.h"
#include "TOFContainer.h"

// Header files passed via #pragma extra_include

// The generated code does not explicitly qualify STL entities
namespace std {} using namespace std;

namespace ROOT {
   static void *new_picoDigit(void *p = nullptr);
   static void *newArray_picoDigit(Long_t size, void *p);
   static void delete_picoDigit(void *p);
   static void deleteArray_picoDigit(void *p);
   static void destruct_picoDigit(void *p);
   static void streamer_picoDigit(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::picoDigit*)
   {
      ::picoDigit *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::picoDigit >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("picoDigit", ::picoDigit::Class_Version(), "picoDigit.h", 21,
                  typeid(::picoDigit), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::picoDigit::Dictionary, isa_proxy, 16,
                  sizeof(::picoDigit) );
      instance.SetNew(&new_picoDigit);
      instance.SetNewArray(&newArray_picoDigit);
      instance.SetDelete(&delete_picoDigit);
      instance.SetDeleteArray(&deleteArray_picoDigit);
      instance.SetDestructor(&destruct_picoDigit);
      instance.SetStreamerFunc(&streamer_picoDigit);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::picoDigit*)
   {
      return GenerateInitInstanceLocal(static_cast<::picoDigit*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::picoDigit*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_picoHit(void *p = nullptr);
   static void *newArray_picoHit(Long_t size, void *p);
   static void delete_picoHit(void *p);
   static void deleteArray_picoHit(void *p);
   static void destruct_picoHit(void *p);
   static void streamer_picoHit(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::picoHit*)
   {
      ::picoHit *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::picoHit >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("picoHit", ::picoHit::Class_Version(), "picoHit.h", 21,
                  typeid(::picoHit), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::picoHit::Dictionary, isa_proxy, 16,
                  sizeof(::picoHit) );
      instance.SetNew(&new_picoHit);
      instance.SetNewArray(&newArray_picoHit);
      instance.SetDelete(&delete_picoHit);
      instance.SetDeleteArray(&deleteArray_picoHit);
      instance.SetDestructor(&destruct_picoHit);
      instance.SetStreamerFunc(&streamer_picoHit);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::picoHit*)
   {
      return GenerateInitInstanceLocal(static_cast<::picoHit*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::picoHit*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *AnalysisClass_Dictionary();
   static void AnalysisClass_TClassManip(TClass*);
   static void *new_AnalysisClass(void *p = nullptr);
   static void *newArray_AnalysisClass(Long_t size, void *p);
   static void delete_AnalysisClass(void *p);
   static void deleteArray_AnalysisClass(void *p);
   static void destruct_AnalysisClass(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AnalysisClass*)
   {
      ::AnalysisClass *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AnalysisClass));
      static ::ROOT::TGenericClassInfo 
         instance("AnalysisClass", "AnalysisClass.h", 28,
                  typeid(::AnalysisClass), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AnalysisClass_Dictionary, isa_proxy, 0,
                  sizeof(::AnalysisClass) );
      instance.SetNew(&new_AnalysisClass);
      instance.SetNewArray(&newArray_AnalysisClass);
      instance.SetDelete(&delete_AnalysisClass);
      instance.SetDeleteArray(&deleteArray_AnalysisClass);
      instance.SetDestructor(&destruct_AnalysisClass);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AnalysisClass*)
   {
      return GenerateInitInstanceLocal(static_cast<::AnalysisClass*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::AnalysisClass*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AnalysisClass_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const ::AnalysisClass*>(nullptr))->GetClass();
      AnalysisClass_TClassManip(theClass);
   return theClass;
   }

   static void AnalysisClass_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_deltaContainer(void *p = nullptr);
   static void *newArray_deltaContainer(Long_t size, void *p);
   static void delete_deltaContainer(void *p);
   static void deleteArray_deltaContainer(void *p);
   static void destruct_deltaContainer(void *p);
   static void streamer_deltaContainer(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::deltaContainer*)
   {
      ::deltaContainer *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::deltaContainer >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("deltaContainer", ::deltaContainer::Class_Version(), "deltaContainer.h", 21,
                  typeid(::deltaContainer), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::deltaContainer::Dictionary, isa_proxy, 16,
                  sizeof(::deltaContainer) );
      instance.SetNew(&new_deltaContainer);
      instance.SetNewArray(&newArray_deltaContainer);
      instance.SetDelete(&delete_deltaContainer);
      instance.SetDeleteArray(&deleteArray_deltaContainer);
      instance.SetDestructor(&destruct_deltaContainer);
      instance.SetStreamerFunc(&streamer_deltaContainer);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::deltaContainer*)
   {
      return GenerateInitInstanceLocal(static_cast<::deltaContainer*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::deltaContainer*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TOFContainer(void *p = nullptr);
   static void *newArray_TOFContainer(Long_t size, void *p);
   static void delete_TOFContainer(void *p);
   static void deleteArray_TOFContainer(void *p);
   static void destruct_TOFContainer(void *p);
   static void streamer_TOFContainer(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TOFContainer*)
   {
      ::TOFContainer *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TOFContainer >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("TOFContainer", ::TOFContainer::Class_Version(), "TOFContainer.h", 21,
                  typeid(::TOFContainer), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TOFContainer::Dictionary, isa_proxy, 16,
                  sizeof(::TOFContainer) );
      instance.SetNew(&new_TOFContainer);
      instance.SetNewArray(&newArray_TOFContainer);
      instance.SetDelete(&delete_TOFContainer);
      instance.SetDeleteArray(&deleteArray_TOFContainer);
      instance.SetDestructor(&destruct_TOFContainer);
      instance.SetStreamerFunc(&streamer_TOFContainer);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TOFContainer*)
   {
      return GenerateInitInstanceLocal(static_cast<::TOFContainer*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::TOFContainer*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr picoDigit::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *picoDigit::Class_Name()
{
   return "picoDigit";
}

//______________________________________________________________________________
const char *picoDigit::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::picoDigit*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int picoDigit::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::picoDigit*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *picoDigit::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::picoDigit*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *picoDigit::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::picoDigit*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr picoHit::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *picoHit::Class_Name()
{
   return "picoHit";
}

//______________________________________________________________________________
const char *picoHit::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::picoHit*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int picoHit::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::picoHit*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *picoHit::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::picoHit*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *picoHit::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::picoHit*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr deltaContainer::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *deltaContainer::Class_Name()
{
   return "deltaContainer";
}

//______________________________________________________________________________
const char *deltaContainer::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::deltaContainer*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int deltaContainer::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::deltaContainer*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *deltaContainer::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::deltaContainer*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *deltaContainer::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::deltaContainer*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TOFContainer::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *TOFContainer::Class_Name()
{
   return "TOFContainer";
}

//______________________________________________________________________________
const char *TOFContainer::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TOFContainer*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int TOFContainer::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TOFContainer*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TOFContainer::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TOFContainer*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TOFContainer::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TOFContainer*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void picoDigit::Streamer(TBuffer &R__b)
{
   // Stream an object of class picoDigit.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      TObject::Streamer(R__b);
      R__b >> fPlane;
      R__b >> fStrip;
      R__b >> fSide;
      R__b >> fTime;
      R__b >> fWidth;
      R__b.CheckByteCount(R__s, R__c, picoDigit::IsA());
   } else {
      R__c = R__b.WriteVersion(picoDigit::IsA(), kTRUE);
      TObject::Streamer(R__b);
      R__b << fPlane;
      R__b << fStrip;
      R__b << fSide;
      R__b << fTime;
      R__b << fWidth;
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_picoDigit(void *p) {
      return  p ? new(p) ::picoDigit : new ::picoDigit;
   }
   static void *newArray_picoDigit(Long_t nElements, void *p) {
      return p ? new(p) ::picoDigit[nElements] : new ::picoDigit[nElements];
   }
   // Wrapper around operator delete
   static void delete_picoDigit(void *p) {
      delete (static_cast<::picoDigit*>(p));
   }
   static void deleteArray_picoDigit(void *p) {
      delete [] (static_cast<::picoDigit*>(p));
   }
   static void destruct_picoDigit(void *p) {
      typedef ::picoDigit current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_picoDigit(TBuffer &buf, void *obj) {
      ((::picoDigit*)obj)->::picoDigit::Streamer(buf);
   }
} // end of namespace ROOT for class ::picoDigit

//______________________________________________________________________________
void picoHit::Streamer(TBuffer &R__b)
{
   // Stream an object of class picoHit.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      TObject::Streamer(R__b);
      R__b >> fPlane;
      R__b >> fStrip;
      R__b >> fTimeT;
      R__b >> fTimeB;
      R__b >> fTimeTB;
      R__b >> fPosX;
      R__b >> fPosY;
      R__b >> fPosZ;
      R__b >> fWidthT;
      R__b >> fWidthB;
      R__b >> fWidthTB;
      R__b >> fCluster;
      R__b.CheckByteCount(R__s, R__c, picoHit::IsA());
   } else {
      R__c = R__b.WriteVersion(picoHit::IsA(), kTRUE);
      TObject::Streamer(R__b);
      R__b << fPlane;
      R__b << fStrip;
      R__b << fTimeT;
      R__b << fTimeB;
      R__b << fTimeTB;
      R__b << fPosX;
      R__b << fPosY;
      R__b << fPosZ;
      R__b << fWidthT;
      R__b << fWidthB;
      R__b << fWidthTB;
      R__b << fCluster;
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_picoHit(void *p) {
      return  p ? new(p) ::picoHit : new ::picoHit;
   }
   static void *newArray_picoHit(Long_t nElements, void *p) {
      return p ? new(p) ::picoHit[nElements] : new ::picoHit[nElements];
   }
   // Wrapper around operator delete
   static void delete_picoHit(void *p) {
      delete (static_cast<::picoHit*>(p));
   }
   static void deleteArray_picoHit(void *p) {
      delete [] (static_cast<::picoHit*>(p));
   }
   static void destruct_picoHit(void *p) {
      typedef ::picoHit current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_picoHit(TBuffer &buf, void *obj) {
      ((::picoHit*)obj)->::picoHit::Streamer(buf);
   }
} // end of namespace ROOT for class ::picoHit

namespace ROOT {
   // Wrappers around operator new
   static void *new_AnalysisClass(void *p) {
      return  p ? new(p) ::AnalysisClass : new ::AnalysisClass;
   }
   static void *newArray_AnalysisClass(Long_t nElements, void *p) {
      return p ? new(p) ::AnalysisClass[nElements] : new ::AnalysisClass[nElements];
   }
   // Wrapper around operator delete
   static void delete_AnalysisClass(void *p) {
      delete (static_cast<::AnalysisClass*>(p));
   }
   static void deleteArray_AnalysisClass(void *p) {
      delete [] (static_cast<::AnalysisClass*>(p));
   }
   static void destruct_AnalysisClass(void *p) {
      typedef ::AnalysisClass current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::AnalysisClass

//______________________________________________________________________________
void deltaContainer::Streamer(TBuffer &R__b)
{
   // Stream an object of class deltaContainer.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      TObject::Streamer(R__b);
      R__b >> fPlaneFirst;
      R__b >> fStripFirst;
      R__b >> fWidthFirst;
      R__b >> fPlaneSecond;
      R__b >> fStripSecond;
      R__b >> fWidthSecond;
      R__b >> fTime;
      R__b >> fPosX;
      R__b >> fpps;
      R__b.CheckByteCount(R__s, R__c, deltaContainer::IsA());
   } else {
      R__c = R__b.WriteVersion(deltaContainer::IsA(), kTRUE);
      TObject::Streamer(R__b);
      R__b << fPlaneFirst;
      R__b << fStripFirst;
      R__b << fWidthFirst;
      R__b << fPlaneSecond;
      R__b << fStripSecond;
      R__b << fWidthSecond;
      R__b << fTime;
      R__b << fPosX;
      R__b << fpps;
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_deltaContainer(void *p) {
      return  p ? new(p) ::deltaContainer : new ::deltaContainer;
   }
   static void *newArray_deltaContainer(Long_t nElements, void *p) {
      return p ? new(p) ::deltaContainer[nElements] : new ::deltaContainer[nElements];
   }
   // Wrapper around operator delete
   static void delete_deltaContainer(void *p) {
      delete (static_cast<::deltaContainer*>(p));
   }
   static void deleteArray_deltaContainer(void *p) {
      delete [] (static_cast<::deltaContainer*>(p));
   }
   static void destruct_deltaContainer(void *p) {
      typedef ::deltaContainer current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_deltaContainer(TBuffer &buf, void *obj) {
      ((::deltaContainer*)obj)->::deltaContainer::Streamer(buf);
   }
} // end of namespace ROOT for class ::deltaContainer

//______________________________________________________________________________
void TOFContainer::Streamer(TBuffer &R__b)
{
   // Stream an object of class TOFContainer.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      TObject::Streamer(R__b);
      R__b >> fPlane;
      R__b >> fStrip;
      R__b >> fTimeL;
      R__b >> fTimeR;
      R__b >> fTime;
      R__b >> fWidthL;
      R__b >> fWidthR;
      R__b >> fWidth;
      R__b.CheckByteCount(R__s, R__c, TOFContainer::IsA());
   } else {
      R__c = R__b.WriteVersion(TOFContainer::IsA(), kTRUE);
      TObject::Streamer(R__b);
      R__b << fPlane;
      R__b << fStrip;
      R__b << fTimeL;
      R__b << fTimeR;
      R__b << fTime;
      R__b << fWidthL;
      R__b << fWidthR;
      R__b << fWidth;
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TOFContainer(void *p) {
      return  p ? new(p) ::TOFContainer : new ::TOFContainer;
   }
   static void *newArray_TOFContainer(Long_t nElements, void *p) {
      return p ? new(p) ::TOFContainer[nElements] : new ::TOFContainer[nElements];
   }
   // Wrapper around operator delete
   static void delete_TOFContainer(void *p) {
      delete (static_cast<::TOFContainer*>(p));
   }
   static void deleteArray_TOFContainer(void *p) {
      delete [] (static_cast<::TOFContainer*>(p));
   }
   static void destruct_TOFContainer(void *p) {
      typedef ::TOFContainer current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_TOFContainer(TBuffer &buf, void *obj) {
      ((::TOFContainer*)obj)->::TOFContainer::Streamer(buf);
   }
} // end of namespace ROOT for class ::TOFContainer

namespace {
  void TriggerDictionaryInitialization_Dict_Impl() {
    static const char* headers[] = {
"AnalysisClass.h",
"deltaContainer.h",
"picoDigit.h",
"picoHit.h",
"TOFContainer.h",
nullptr
    };
    static const char* includePaths[] = {
"/usr/include",
"/home/alex/MyGitlab/picoTDC/macro/ana/",
nullptr
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "Dict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$picoDigit.h")))  __attribute__((annotate("$clingAutoload$AnalysisClass.h")))  picoDigit;
class __attribute__((annotate("$clingAutoload$picoHit.h")))  __attribute__((annotate("$clingAutoload$AnalysisClass.h")))  picoHit;
class __attribute__((annotate("$clingAutoload$AnalysisClass.h")))  AnalysisClass;
class __attribute__((annotate("$clingAutoload$deltaContainer.h")))  deltaContainer;
class __attribute__((annotate("$clingAutoload$TOFContainer.h")))  TOFContainer;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "Dict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "AnalysisClass.h"
#include "deltaContainer.h"
#include "picoDigit.h"
#include "picoHit.h"
#include "TOFContainer.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"AnalysisClass", payloadCode, "@",
"TOFContainer", payloadCode, "@",
"deltaContainer", payloadCode, "@",
"picoDigit", payloadCode, "@",
"picoHit", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("Dict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_Dict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_Dict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_Dict() {
  TriggerDictionaryInitialization_Dict_Impl();
}
