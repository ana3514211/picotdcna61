/*
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
*/

/*
   File:   picoDigit.h
   Author: avdmit

   Created on January 28, 2020, 10:53 AM
*/

#ifndef PICODIGIT_H
#define PICODIGIT_H

#include "TObject.h"

using namespace std;

class picoDigit : public TObject {
public:
  /** Default constructor **/
  picoDigit();

  /** Constructor to use **/
  picoDigit(short iPlane, short iStrip, short iSide,
            double iTime, double iWidth);

  void SetPlane(short tmp)   { fPlane = tmp;  }
  void SetStrip(short tmp)   { fStrip = tmp;  }
  void SetSide(short tmp)    { fSide = tmp;   }
  void SetTime(double tmp)   { fTime = tmp;   }
  void SetWidth(double tmp)  { fWidth = tmp;  }

  short GetPlane()  { return fPlane;  }
  short GetStrip()  { return fStrip;  }
  short GetSide()   { return fSide;   }
  double GetTime()  { return fTime;   }
  double GetWidth() { return fWidth;  }

  /** Destructor **/
  virtual ~picoDigit() {};

private:
  short fPlane;
  short fStrip;
  short fSide;
  double fTime;
  double fWidth;

  ClassDef(picoDigit, 1);
};

#endif /* PICODIGIT_H */
